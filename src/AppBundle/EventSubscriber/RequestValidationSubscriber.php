<?php
	namespace AppBundle\EventSubscriber;

	use AppBundle\Controller\RequestValidationController;
	use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
	use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
	use Symfony\Component\EventDispatcher\EventSubscriberInterface;
	use Symfony\Component\HttpKernel\KernelEvents;

	class RequestValidationSubscriber implements EventSubscriberInterface
	{
		
		/** S'assure que le host du referer est le meme que celui de l'application
		*/
		public function onKernelController(FilterControllerEvent $event)
		{				
			$controller = $event->getController();

			// Ne s'applique qu'aux controllers necessitant validation du referer (API Rest)
			if ($controller[0] instanceof RequestValidationController) {
				
				$host = $event->getRequest()->getHost();					// Recuperation du host actuel
				$referer = $event->getRequest()->headers->get('referer');	// Recuperation du referer
				$refererHost = parse_url($referer, PHP_URL_HOST);			// Recuperation du host du referer
				
				if(substr($refererHost, 0 - strlen($host)) !== $host) {
					throw new AccessDeniedHttpException("Vous n'avez pas accès à cette application.");
				}
			}
		}

		public static function getSubscribedEvents()
		{
			return array(
				KernelEvents::CONTROLLER => 'onKernelController',
			);
		}
	}
?>