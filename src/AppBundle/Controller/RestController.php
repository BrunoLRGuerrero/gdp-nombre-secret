<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Service\NombreSecret;
use AppBundle\Controller\RequestValidationController;

/**
 * @Route("/game")
 */
class RestController extends Controller implements RequestValidationController
{
	private $nombreSecret;
	
	public function __construct(NombreSecret $nombreSecret)
	{
		$this->nombreSecret = $nombreSecret;
	}
	
    /** Effectue une tentative de nombre
     * @Route("/try/{number}", requirements={"number" = "\d+"})
	 * @Method("GET")
     */
    public function tryAction(Request $request)
    {
		$response = [];
		$result = $this->nombreSecret->try($request->get("number"));
		$resultText = "";
		
		switch($result) {
			case(-1) :
				$resultText = "C'est moins.";
				break;
			case(0) :
				$resultText = "Nombre trouvé !";
				break;
			case(1) :
				$resultText = "C'est plus.";
				break;
		}
		
		$response["result"] = $result;
		$response["resultText"] = $resultText;

        return new JsonResponse($response);
    }
}
