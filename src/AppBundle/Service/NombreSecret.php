<?php
	namespace AppBundle\Service;

	use Symfony\Component\HttpFoundation\Session\Session;
	use Symfony\Component\HttpFoundation\Session\SessionInterface;
	
	class NombreSecret {
	
		private $session;
	
		public function __construct(SessionInterface $session)
		{
			$this->session = $session;
		}
			
		/** Tentative de réponse par l'utilisateur
		*/
		public function try(int $number) {
			if(empty($this->getAnswer())) {
				$this->initialize();
			}
			
			$result = ($this->getAnswer() <=> $number);
			
			if($result === 0) {
				$this->endSession();
			}

			return $result;
		}
	
		/** Crée une nouvelle instance de jeu
		*/
		private function initialize() {
			$this->session->set("number", mt_rand(0, 100));
		}
	
		/** Termine l'instance de jeu
		*/
		private function endSession() {
			$this->session->remove("number");
		}
	
		/** Récupère la réponse attendue 
		*/
		private function getAnswer() {
			return $this->session->get("number");
		}
	}
	
?>

