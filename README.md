GDP - Nombre Mystère

========================

## Démarche

Avant de réaliser cet exercice, quelques heures ont été consacrées la veille à l'étude des deux frameworks utilisés dans ce projet : Symfony 3, et Vue.js 2.
Ainsi, bien que cet exercice ait pris 4 heures au final, 3 autres heures ont été nécessaires pour pouvoir correctement appréhender les technos requises, et installer les outils nécessaires au bon déroulement de l'exercice.
Une fois cette session de découverte effectuée, le projet de test a intégralement été supprimé afin d'éviter les tentations de copier-coller.

## Améliorations possibles 

Parmi les améliorations envisageables sur ce projet :

### Scincder complètement la partie front de la partie back 
Pour permettre à l'API d'être utilisable depuis n'importe quel client, il aurait intéressant de dissocier la partie client de la partie serveur. Pour des contraintes de temps et d'outils non appréhendés, cette solution n'a pas été reteue, et l'interface de l'application est tout de meme servie par Symfony.

### Utiliser un bundler plutôt que les CDN 
Aux vues de la taille de l'application et du temps qu'il a été dédié, les installations d'outils tels qu'npm ou webpack ont été mis de côté au profit de l'exploitation des CDN de vue, vue-resource et bootstrap.

### Faire de l'interface et du tableau un (ou deux) composants
Il aurait été plus pertinent de créer des components pour l'interface plutôt que de tout injecter dans le html principal. Pour des raison liées au binding avec Symfony (Impliquant un réécriture d'URL) et l'absence de packager, cette solution a également été laissée de côté.
